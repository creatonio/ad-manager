import React, { Component } from 'react';

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {isOpened: false};
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
  }

  handleOutsideClick(e) {
    if(this.node && !this.node.contains(e.target)) {
      this.setState({ isOpened: false });
    }
  }

  handleClick() {
    if (!this.state.isOpened) {
      document.addEventListener('click', this.handleOutsideClick, false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick, false);
    }

    this.setState({ isOpened: !this.state.isOpened });
  }

  render() {
    return (
      <div className="btn-group btn-sm">
        <div className="dropdown show" ref={node => this.node = node}>
          <button
            className="btn btn-secondary btn-sm dropdown-toggle"
            type="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
            onClick={this.handleClick.bind(this)}>
            {this.props.title}
          </button>

          <div className={'dropdown-menu ' + (this.state.isOpened ? 'show' : '')} aria-labelledby="dropdownMenuLink">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}

const DropdownItem = props => <div className="dropdown-item">{props.children}</div>

export {Dropdown, DropdownItem};
