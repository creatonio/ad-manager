import React from 'react';

const Badge = ({status}) => {

  const badgeStyles = {
    'ACTIVE': 'badge-primary',
    'INACTIVE': 'badge-secondary'
  };

  return <span className={`badge ${badgeStyles[status]}`}>{status}</span>
};

export default Badge;
