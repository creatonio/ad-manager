import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import * as actions from '../../../actions/actions';
import { Dropdown, DropdownItem } from "../../shared/Dropdown";


import { CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis
} from 'recharts';

class Campaign extends Component{

  componentWillMount() {
    const campaignId = this.props.match.params.campaignId;
    this.props.fetchCampaign(campaignId);
  }

  // Dirty-dirty hack to make component re-render when route changed.
  // Need to fix it ASAP
  componentWillReceiveProps (nextProps) {
    const campaignId = nextProps.match.params.campaignId;
    const currentPath = `${this.props.location.pathname}${this.props.location.search}`;
    const nextPath = `${nextProps.location.pathname}${nextProps.location.search}`;

    if (currentPath === nextPath) {
      return;
    }

    this.props.fetchCampaign(campaignId);
  }


  render() {
    if (this.isFetching) {
      return <div>Loading...</div>;
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h2>{this.props.currentCampaign.name}</h2>
          </div>
          <div className="col">
            <Dropdown title={'Change Campaign'}>
              {this.props.allCampaigns.map(campaign => {
                return (
                  <DropdownItem key={campaign.id}>
                    <Link to={`/campaigns/${campaign.id}`} className="text-dark">{campaign.name}</Link>
                  </DropdownItem>
                )
              })}
            </Dropdown>
            <button type="button" className="btn btn-sm btn-light">
              <Link to='/campaigns' className='text-dark'>All campaigns</Link>
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <ResponsiveContainer width='100%' height={350}>
              <LineChart data={this.props.stats} margin={{ top: 35, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="date" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="clicks" stroke="#8884d8" />
                <Line type="monotone" dataKey="impressions" stroke="#82ca9d" />
              </LineChart>
            </ResponsiveContainer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({campaign, campaignList}) => ({
  isFetching: campaign.isFetching,
  stats: campaign.stats,
  allCampaigns: campaignList.list,
  currentCampaign: campaign.selected
});
export default connect(mapStateToProps, actions)(Campaign);


