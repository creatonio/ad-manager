import React, { Component } from 'react';
import {connect} from "react-redux";
import {Dropdown, DropdownItem} from '../shared/Dropdown';
import Badge from '../shared/Badge';
import * as actions from '../../actions/actions';
import {Link} from "react-router-dom";

class Campaigns extends Component {

  changeCampaignStatusRender({status, id}) {
    const statusMap = {
      'ACTIVE': 'Deactivate',
      'INACTIVE': 'Activate'
    };

    return <a onClick={() => this.props.changeCampaignStatus(status, id)}>{statusMap[status]}</a>
  }

  /**
   * Better to use some localization library for such operations.
   * @param value
   * @param currency
   * @return {string}
   */
  formatCurrency(value, currency = '$') {
    return currency + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
  }

  /**
   * Render table body for campaign list
   * @param campaign list
   * return <tr>[] elements
   */
  renderCampaignList(campaigns) {
    return campaigns.map(campaign => {
      const {id, status, name, daily_budget, total_budget} = campaign;

      return (
        <tr key={id}>
          <td>
            <Badge status={status} />
          </td>
          <td>{name}</td>
          <td>{this.formatCurrency(daily_budget)}</td>
          <td>{this.formatCurrency(total_budget)}</td>
          <td>
            <Dropdown key={id} title={'Actions'}>
              <DropdownItem>{this.changeCampaignStatusRender(campaign)}</DropdownItem>
              <DropdownItem><Link to={`/campaigns/${id}`} className="text-dark">Stats</Link></DropdownItem>
            </Dropdown>
          </td>
        </tr>
      );
    })
  }

  componentWillMount(){
    this.props.fetchCampaingList();
  }

  render() {
    const campaigns = this.renderCampaignList(this.props.campaigns);

    if (this.props.isFetching) {
      return <div>Loading...</div>
    }

    return (
      <div className="container">
        <div className="row">
          <table className="table table-bordered">
            <thead>
            <tr>
              <th>Status</th>
              <th>Name</th>
              <th>Daily Budget</th>
              <th>Total Budget</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
              {campaigns}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({campaignList}) => ({isFetching: campaignList.isFetching, campaigns: campaignList.list});
export default connect(mapStateToProps, actions)(Campaigns);
