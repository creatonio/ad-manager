import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter, routerMiddleware } from 'react-router-redux'

import reducers from '../../reducers';
import { saveState, getState } from "../../helpers/steteLocalStorage";

import Jumbotron from '../Jumbotron/Jumbotron';
import CampaignList from "../Campaigns/CampaignList";
import Campaign from "../Campaigns/Campaign/Campaign";

const history = createHistory();
const initialState = getState();
const store = createStore(
  reducers,
  initialState,
  applyMiddleware(thunk, routerMiddleware(history)),
);

store.subscribe(() => saveState(store.getState()));

/**
 * Not sure about Redirect It seems need more time to React router 4
 */
const App = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Jumbotron />
        <Switch >
          <Redirect exact from='/' to='/campaigns'/>
          <Route path='/campaigns' exact component={CampaignList} />
          <Route path='/campaigns/:campaignId' component={Campaign} />
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>
);

export default App;
