import * as types from "../actions/types";
import campaignListReducer from './campaignList.reducer';

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(campaignListReducer(undefined, {})).toEqual({
      isFetching: false,
      list: []
    });
  });

  it('should handle FETCH_CAMPAIGN_LIST_SUCCESSED', () => {
    expect(campaignListReducer({}, {
      type: types.FETCH_CAMPAIGN_LIST_SUCCESSED,
      campaigns: [{}, {}]
    })).toEqual({
      isFetching: false,
      list: [{}, {}]
    });
  });

  it('should FETCH_CAMPAIGN_LIST_START and FETCH_CAMPAIGN_LIST_FAILED', () => {
    expect(campaignListReducer({}, {
      type: types.FETCH_CAMPAIGN_LIST_START
    })).toEqual({
      isFetching: true
    });

    expect(campaignListReducer({}, {
      type: types.FETCH_CAMPAIGN_LIST_FAILED
    })).toEqual({
      isFetching: true
    });
  });
});
