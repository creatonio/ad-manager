import {
  FETCH_CAMPAIGN_START,
  FETCH_CAMPAIGN_FAILED,
  FETCH_CAMPAIGN_SUCCESSED,
  SET_CURRENT_CAMPAIGN
} from "../actions/types";

const initialState = {
  isFetching: false,
  stats: [],
  selected: {}
};

function campaignReducer (state = initialState, action) {
  switch (action.type) {
    case FETCH_CAMPAIGN_SUCCESSED:
      return {
        ...state,
        stats: action.stats,
        isFetching: false
      };
    case FETCH_CAMPAIGN_FAILED:
    case FETCH_CAMPAIGN_START:
      return {
        ...state,
        isFetching: true
      };
    case SET_CURRENT_CAMPAIGN:
      return {
        ...state,
        selected: action.selected[0]
      };
    default:
      return state;
  }
}

export default campaignReducer
