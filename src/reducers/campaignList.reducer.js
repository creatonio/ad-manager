import {
  FETCH_CAMPAIGN_LIST_START,
  FETCH_CAMPAIGN_LIST_SUCCESSED,
  FETCH_CAMPAIGN_LIST_FAILED
} from "../actions/types";

const initialState = {
  isFetching: false,
  list: []
};

function campaignListReducer (state = initialState, action) {
  switch (action.type) {
    case FETCH_CAMPAIGN_LIST_SUCCESSED:
      return {
        ...state,
        list: action.campaigns,
        isFetching: false
      };
    case FETCH_CAMPAIGN_LIST_FAILED:
    case FETCH_CAMPAIGN_LIST_START:
      return {
        ...state,
        isFetching: true
      };
    default:
      return state;
  }
}

export default campaignListReducer
