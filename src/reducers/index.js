import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import campaignListReducer from "./campaignList.reducer";
import campaignReducer from "./campaign.reducer";

const rootReducer = combineReducers({
  router: routerReducer,
  campaignList: campaignListReducer,
  campaign: campaignReducer
});

export default rootReducer;
