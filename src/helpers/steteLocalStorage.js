export const saveState = (state) => {
  if (localStorage) {
    return localStorage.setItem('state', JSON.stringify(state));
  }
};

export const getState = () => (
  localStorage && localStorage.getItem('state') ? JSON.parse(localStorage.getItem('state')) : {}
);
