import * as types from './types';

const API_URL = 'https://5cd3f999-f49f-4e42-8b8b-173c7185f093.mock.pstmn.io/campaigns';

const fetchingStart = (dispatch, type) => dispatch({type});
const fetchingFailed = (dispatch, type) => dispatch({type});

const getCurrentCampaign = (campaigns, id) => {
  return campaigns.filter(campaign => {
    return campaign.id === id;
  });
};

export function fetchCampaingList () {
  return (dispatch) => {
    fetchingStart(dispatch, types.FETCH_CAMPAIGN_LIST_START);

    return fetch(API_URL)
      .then(response => response.json())
      .then(campaigns => dispatch({type: types.FETCH_CAMPAIGN_LIST_SUCCESSED, campaigns}))
      .catch(() => fetchingFailed(dispatch, types.FETCH_CAMPAIGN_LIST_FAILED));
  }
}

export function fetchCampaign(campaignId) {
  return (dispatch, getState) => {
    fetchingStart(dispatch, types.FETCH_CAMPAIGN_START);

    return fetch(`${API_URL}/${campaignId}/stats`)
      .then(response => response.json())
      .then(stats => {
        const campaigns = getState().campaignList.list;
        dispatch({type: types.FETCH_CAMPAIGN_SUCCESSED, stats});
        dispatch({type: types.SET_CURRENT_CAMPAIGN, selected: getCurrentCampaign(campaigns, campaignId)});
      })
      .catch(() => fetchingFailed(dispatch, types.FETCH_CAMPAIGN_FAILED));
  }
}

export function changeCampaignStatus(status, campaignId) {
  return function (dispatch) {
    const statusChangeUrl = {
      'ACTIVE': `${API_URL}/${campaignId}/deactivate`,
      'INACTIVE': `${API_URL}/${campaignId}/activate`,
    };

    const request = {
      method: 'POST'
    };

    return fetch(statusChangeUrl[status], request)
      .then(() => {
        dispatch({type: types.CAMPAIGN_CHANGE_STATUS_SUCCESSED})
      });
  }
}
