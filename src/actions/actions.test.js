import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';

import * as actions from './actions'
import * as types from './types';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {

  it('should creates FETCH_CAMPAIGN_LIST_SUCCESSED when fetching campaigns is done', () => {
    fetch.mockResponse(JSON.stringify([{}, {}]));

    const expectedActions = [
      { type: types.FETCH_CAMPAIGN_LIST_START },
      { type: types.FETCH_CAMPAIGN_LIST_SUCCESSED, campaigns: [{}, {}] }
    ];

    const store = mockStore({ campaignList: { list: []} });

    return store.dispatch(actions.fetchCampaingList())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
  });

  it('should creates FETCH_CAMPAIGN_LIST_FAILED when response failed', () => {

    fetch.mockReject(new Error('fake error message'));
    const store = mockStore({});

    const expectedActions = [
      { type: types.FETCH_CAMPAIGN_LIST_START },
      { type: types.FETCH_CAMPAIGN_LIST_FAILED }
    ];

    return store.dispatch(actions.fetchCampaingList())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('should creates FETCH_CAMPAIGN_LIST_SUCCESSED when fetching campaigns was done', () => {

    fetch.mockResponse(JSON.stringify([]));

    const expectedActions = [
      { type: types.FETCH_CAMPAIGN_START },
      { type: types.FETCH_CAMPAIGN_SUCCESSED, stats: [] },
      { type: types.SET_CURRENT_CAMPAIGN, selected: [] },
    ];

    const store = mockStore({
      campaign: { stats: []},
      campaignList: {
        list: []
      }
    });

    return store.dispatch(actions.fetchCampaign())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
  });

});
